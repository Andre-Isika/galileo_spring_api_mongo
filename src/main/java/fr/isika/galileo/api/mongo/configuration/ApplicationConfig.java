package fr.isika.galileo.api.mongo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.isika.galileo.api.mongo.service.LivreService;
import fr.isika.galileo.api.mongo.service.LivreServiceImpl;

@Configuration
@ComponentScan("fr.isika.galileo.api.mongo")
public class ApplicationConfig {

    @Bean(name="livreService")
    public LivreService getTopoService() {
        return new LivreServiceImpl();
    }

}
