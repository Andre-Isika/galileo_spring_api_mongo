package fr.isika.galileo.api.mongo.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import fr.isika.galileo.api.mongo.models.Commentaire;
import fr.isika.galileo.api.mongo.models.Livre;
import fr.isika.galileo.api.mongo.requetes.CommentaireForm;
import fr.isika.galileo.api.mongo.service.LivreService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/livres")
public class LivreController {



    @Autowired
    @Qualifier(value = "livreService")
    LivreService service;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * Methode de recherche de tous les livres.
     * @return
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Collection<Livre> getAll() {
        System.out.println("-------> : récupérer tous les livres");
        logger.debug("Récupération de tous les livres");
        return service.getAllLivres();
    }
    
    /**
     * Methode de recherche d'un livre par son titre.
     * @param title
     * @return
     */
    @GetMapping("/titre/{titre}")
    @ResponseStatus(HttpStatus.OK)
    public Collection<Livre> getTitle(@PathVariable(value= "titre") String titre) {
        System.out.println("-------> : récupération de titre demandé");
        logger.debug("Récupération de tous les livres", titre);
        return service.findByTitle(titre);
    }

    /**
     * Méthode de recherche de livre par id
     * @param id
     * @return
     */
    @GetMapping("id/{id}")
    public Optional<Livre> getById(@PathVariable(value= "id") String id) {
        logger.debug("récupération d'un livre avec livre-id= {}.", id);
        return service.findById(id);
    }
    
    /**
     * Méthode de recherche d'une listre livre par id
     * @param 
     * @return
     */
    @GetMapping("/liste")
    public Collection<Livre> getAllById(@RequestBody List<String> maListe) {
    	List<Livre> mesLivres = new ArrayList<Livre>();
        
        for(String monId : maListe) {
        	Livre liv = service.findById(monId).orElse(liv = new Livre());
        	mesLivres.add(liv);
        }
        return mesLivres;
    }

    /**
     * Methode de modification de livre par id.
     * @param id
     * @param user
     * @return
     */
    @PutMapping("/commentaire")
    @ResponseStatus(HttpStatus.OK)
    public String update(@RequestBody CommentaireForm commentaire) {
    	System.out.println(commentaire.getCommentaire());
        
        Livre livre = service.findById(commentaire.getIdLivre()).orElse( new Livre() );
     System.out.println(livre.getTitle()+ " "+ commentaire.getUsername() + " " + commentaire.getCommentaire());
        Commentaire monCom = new Commentaire(commentaire.getUsername(), commentaire.getCommentaire(), livre.getCommententaires().size());
        livre.getCommententaires().add(monCom);
        service.updateLivre(livre);
        return "commentaire de  " + commentaire.getUsername() + " a bien été ajouté !";
    }

    /**
     * Méthode de suppression de livre par id
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String delete(@PathVariable(value= "id") int id) {
        logger.debug("Effacer avec livre-id= {}.", id);
        service.deleteLivreById(id);
        return "le livre d'id " + id + " a bien été supprimé.";
    }

    /**
     * Méthode de suppression de tous livres
     * @return
     */
    @DeleteMapping(value= "/toutSupprimer")
    public String deleteAll() {
        logger.debug("Supprimer tout les livres");
        service.deleteAllLivres();
        return "Tous les livres sont supprimés";
    }
    
    /**
     * Méthode de recherche d'une listre livre par id
     * @param 
     * @return
     */
    @PostMapping("/mes-livres")
    public Collection<Livre> getAllBook(@RequestBody List<String> maListe) {
    	List<Livre> mesLivres = new ArrayList<Livre>();
        
        for(String monId : maListe) {
        	Livre liv = service.findById(monId).orElse(liv = new Livre());
        	if(liv.getTitle().length() !=0) {        	
        	mesLivres.add(liv);
   
        	}
        }
        return mesLivres;
    }

}