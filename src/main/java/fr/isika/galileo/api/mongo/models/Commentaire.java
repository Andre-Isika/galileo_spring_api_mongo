package fr.isika.galileo.api.mongo.models;

public class Commentaire {
	
	private String username;
	private String commentaire;
	private int id;


	public Commentaire(String username, String commentaire, int id) {

		this.username = username;
		this.commentaire = commentaire;
		this.id = id;
	}
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	

}
