package fr.isika.galileo.api.mongo.dao;


import java.util.Collection;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import fr.isika.galileo.api.mongo.models.Livre;

@Repository
public interface LivreDao extends MongoRepository<Livre, Integer>{


	Collection<Livre> findByTitleLike(String title);
	
	Optional<Livre> findById(String id);



}
