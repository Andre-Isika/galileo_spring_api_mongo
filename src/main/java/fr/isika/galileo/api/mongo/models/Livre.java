package fr.isika.galileo.api.mongo.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;


@Document(collection= "livres")

public class Livre {

    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    private ObjectId _id;
    private String title;
    private String subtitle;
    private List<String> authors;
    private String publisher;
    private String publishedDate;
    private String description;
   // private List<String> industryIdentifiers;
    private int pageCount;
    private String printType;
    private List<String> categories;
    private int averageRating;
    private int ratingsCount;
    private String maturityRating;
    private String language;
    private String id;
    private String link;
    private String thumbnail;
    private List<Commentaire> commententaires = new ArrayList<Commentaire>();
    
    
    public List<Commentaire> getCommententaires() {
		return commententaires;
	}

	public void setCommententaires(List<Commentaire> commententaires) {
		this.commententaires = commententaires;
	}

	public List<Integer> getLikes() {
		return likes;
	}

	public void setLikes(List<Integer> likes) {
		this.likes = likes;
	}

	private List<Integer> likes;
  
   // private List<String> images;
    
    
    
 

    @Override
    public String toString() {
        return "Livre{" + "id=" + id + ", titre='" + title + '\'' + ", sous-titre='" + subtitle + '\'' + ", "
        		+ "auteurs=" + authors.toString() + ", editeur=" + publisher + ", date de publication=" + publishedDate + ", nombre de page=" + pageCount + ", catégorie=" + categories.toString() + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Livre livre = (Livre) o;
        return pageCount == livre.pageCount && Objects.equals(_id, livre._id) && Objects.equals(id, livre.id) && Objects.equals(title,
                                                                                    livre.title) && Objects.equals(subtitle,
                                                                                                                        livre.subtitle) && Objects
                .equals(description, livre.description) && Objects.equals(link, livre.link) && Objects.equals(thumbnail,
                                                                                                                  livre.thumbnail) && Objects
                .equals(publisher, livre.publisher) && /*Objects.equals(images,
                livre.images)&& */Objects.equals(language,
                                                    livre.language) && Objects
.equals(maturityRating, livre.maturityRating) && Objects.equals(ratingsCount, livre.ratingsCount) && Objects.equals(averageRating,
                                              livre.averageRating) && Objects.equals(printType, livre.printType)/* && Objects.equals(industryIdentifiers, livre.industryIdentifiers)*/;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, subtitle, authors, publisher, publishedDate, description,/* industryIdentifiers,*/ pageCount, printType, categories, averageRating, ratingsCount,
        		maturityRating, language, link, thumbnail/*, images*/);
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*public List<String> getIndustryIdentifiers() {
		return industryIdentifiers;
	}

	public void setIndustryIdentifiers(List<String> industryIdentifiers) {
		this.industryIdentifiers = industryIdentifiers;
	}*/

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public String getPrintType() {
		return printType;
	}

	public void setPrintType(String printType) {
		this.printType = printType;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public int getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(int averageRating) {
		this.averageRating = averageRating;
	}

	public int getRatingsCount() {
		return ratingsCount;
	}

	public void setRatingsCount(int ratingsCount) {
		this.ratingsCount = ratingsCount;
	}

	public String getMaturityRating() {
		return maturityRating;
	}

	public void setMaturityRating(String maturityRating) {
		this.maturityRating = maturityRating;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getIdMongo() {
		return id;
	}

	public void setIdMongo(String idMongo) {
		this.id = idMongo;
	}
	
    public ObjectId getId() {
        return _id;
    }

    public Livre setId(ObjectId _id) {
        this._id = _id;
        return this;
    }

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}



	/*public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}*/
    
   
}
