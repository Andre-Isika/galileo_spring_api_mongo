package fr.isika.galileo.api.mongo.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import fr.isika.galileo.api.mongo.models.Livre;

public interface LivreService {



    public void createLivre(List<Livre> livre);


    public Collection<Livre> getAllLivres();


    public Optional<Livre> findById(String id);


    public void deleteLivreById(int id);


    public void updateLivre(Livre livre);


    public void deleteAllLivres();
    
    public Collection<Livre> findByTitle(String title);


}
