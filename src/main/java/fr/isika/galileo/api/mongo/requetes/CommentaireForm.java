package fr.isika.galileo.api.mongo.requetes;

public class CommentaireForm {
	
	private String idLivre;
	private String username;
	private String commentaire;
	
	
	public String getIdLivre() {
		return idLivre;
	}
	public void setIdLivre(String idLivre) {
		this.idLivre = idLivre;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	
	

}
