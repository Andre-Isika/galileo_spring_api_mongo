package fr.isika.galileo.api.mongo.service;

import org.springframework.beans.factory.annotation.Autowired;

import fr.isika.galileo.api.mongo.dao.LivreDao;
import fr.isika.galileo.api.mongo.models.Livre;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class LivreServiceImpl implements LivreService {

    @Autowired
    LivreDao dao;


    @Override
    public void createLivre(List<Livre> livre) {
        dao.saveAll(livre);
    }


    @Override
    public Collection<Livre> getAllLivres() {
        return dao.findAll();
    }



    @Override
    public void deleteLivreById(int id) {
        dao.deleteById(id);
    }


    @Override
    public void updateLivre(Livre livre) {
        dao.save(livre);
    }


    @Override
    public void deleteAllLivres() {
        dao.deleteAll();
    }


	@Override
	public Collection<Livre> findByTitle(String title) {
		
		return dao.findByTitleLike(title);
	}


	@Override
	public Optional<Livre> findById(String id) {

		return dao.findById(id);
	}


    
    
}

